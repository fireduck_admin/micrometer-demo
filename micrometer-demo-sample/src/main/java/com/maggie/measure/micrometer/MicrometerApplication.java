package com.maggie.measure.micrometer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: micrometer-demo
 * @description: 启动类
 */
@SpringBootApplication
public class MicrometerApplication {
    public static void main(String[] args) {
        SpringApplication.run(MicrometerApplication.class, args);
    }
}


package com.maggie.measure.micrometer.controller;

import com.maggie.measure.micrometer.service.RedisOpsValService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisTestController {

    @Autowired
    private RedisOpsValService redisOpsValService;


    @RequestMapping("/get")
    public String get() {
        System.out.println("执行 redis get 操作");
        for (int i = 0; i < 5; i++) {
            if (i == 4) {
                for (int j = 0; j < 5; j++) {
                    redisOpsValService.get("key" + i);
                }
            }
            redisOpsValService.get("key" + i);
        }
        return "success";
    }

    @RequestMapping("/set")
    public String set() {
        System.out.println("执行 redis set 操作");
        for (int i = 0; i < 5; i++) {
            if (i == 2) {
                for (int j = 0; j < 10; j++) {
                    redisOpsValService.set("set" + i, "value_" + i);
                }
            }
            redisOpsValService.set("set" + i, "value_" + i);
        }
        return "success";
    }
}

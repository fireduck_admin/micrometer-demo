package com.maggie.measure.micrometer.metric;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.MeterBinder;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class RedisMetric implements MeterBinder {

    public static AtomicInteger atomicGetInteger = new AtomicInteger(0);
    public static AtomicInteger atomicSetInteger = new AtomicInteger(0);
    public static ConcurrentHashMap<String, Map<String, Object>> param = new ConcurrentHashMap(16);

    @Override
    public void bindTo(MeterRegistry meterRegistry) {
        Gauge.builder("redis.get.info", atomicGetInteger, c -> c.getAndIncrement())
                .tags("host", "localhost")
                .description("测试 redis get info")
                .register(meterRegistry);
        Gauge.builder("redis.set.info", atomicSetInteger, c -> c.getAndIncrement())
                .tags("host", "localhost")
                .description("测试 redis set info")
                .register(meterRegistry);
    }

}
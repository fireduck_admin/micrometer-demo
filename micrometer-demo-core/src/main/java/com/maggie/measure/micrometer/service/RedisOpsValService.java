package com.maggie.measure.micrometer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @program: micrometer-demo
 * @description: redis操作封装工具类, 由于 redistemplate 无法切面获取方法,只能封装一层
 */
@Service
public class RedisOpsValService {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * Set {@code value} for {@code key}.
     *
     * @param key   must not be {@literal null}.
     * @param value must not be {@literal null}.
     * @see <a href="https://redis.io/commands/set">Redis Documentation: SET</a>
     */
    public void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }


    /**
     * Get the value of {@code key}.
     *
     * @param key must not be {@literal null}.
     * @return {@literal null} when used in pipeline / transaction.
     * @see <a href="https://redis.io/commands/get">Redis Documentation: GET</a>
     */
    public String get(Object key) {
        return redisTemplate.opsForValue().get("1");
    }
}

package com.maggie.measure.micrometer.endpoint;

import com.alibaba.fastjson.JSONObject;
import com.maggie.measure.micrometer.metric.RedisMetric;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * @program: micrometer-demo
 * @description: redis监控断点
 */
@Component
@Endpoint(id = "redis")
public class RedisRegistryEndpoint {
    private final MeterRegistry registry;

    public RedisRegistryEndpoint(MeterRegistry registry) {
        this.registry = registry;
    }

    @ReadOperation
    public String home() {
        Set<String> set = new HashSet<>();
        set.add("redis.get.info");
        set.add("redis.set.info");
        return JSONObject.toJSONString(set);
    }

    @ReadOperation
    public String metric(@Selector String tagName) {
        tagName = tagName.replaceAll("\\.", "")
                .replaceAll("redis", "").replaceAll("info", "");
        return JSONObject.toJSONString(RedisMetric.param.get(tagName));
    }
}
